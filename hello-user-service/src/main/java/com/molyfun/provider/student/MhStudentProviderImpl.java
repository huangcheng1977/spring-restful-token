package com.molyfun.provider.student;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import com.molyfun.core.base.BaseProviderImpl;
import com.molyfun.core.support.dubbo.spring.annotation.DubboService;
import com.molyfun.core.util.RedisUtil;
import com.molyfun.dao.student.MhStudentMapper;
import com.molyfun.model.student.MhStudent;



@DubboService(interfaceClass = MhStudentProvider.class)
@CacheConfig(cacheNames = "mhStudent")
public class MhStudentProviderImpl extends BaseProviderImpl<MhStudent> implements MhStudentProvider {
	
	@Autowired
	private MhStudentMapper mhStudentMapper;
    
	@Override
	public MhStudent queryByCardId(String cardId) {
		try {
			List<MhStudent> list=mhStudentMapper.selectByCardId(cardId);
			if(list.size()>0){
				MhStudent student=list.get(0);
				String key=getCacheKey(student.getId());
				RedisUtil.set(key,student);
				return student;
			}
			return null;
		} catch (Exception e) {
		    throw new RuntimeException(e.getMessage(),e);
		}
	}

  
	@Override
	public MhStudent update(MhStudent stu){
		
		try {
			mhStudentMapper.add(stu);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(),e);
		}
		return null;
	}


	@Override
	public String findInviteCode(String cardId) {
		try {
			String inviteStr=mhStudentMapper.getInviteCode(cardId);
			return inviteStr;
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(),e);
		}
		
	}

	

	



	

	

}
