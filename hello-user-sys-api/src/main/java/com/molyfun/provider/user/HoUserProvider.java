package com.molyfun.provider.user;

import com.molyfun.core.base.BaseProvider;
import com.molyfun.model.student.MhStudent;
import com.molyfun.model.user.HoUser;

/**
 * 作者：石冬生

 * 创建日期：2016-7-13

 */
public interface HoUserProvider extends BaseProvider<HoUser> {
	
	
	public HoUser queryUserByUserNameAndPhone(HoUser user);

	public HoUser queryByPhone(String phone);

	/**
	 * @param name
	 * @return 
	 */
	public HoUser queryByName(String name);

	/**
	 * @param token
	 * @return
	 */
	public String queryUserIdByToken(String token);
	

}
