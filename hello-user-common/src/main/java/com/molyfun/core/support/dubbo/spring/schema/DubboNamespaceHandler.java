package com.molyfun.core.support.dubbo.spring.schema;


import com.alibaba.dubbo.config.spring.schema.DubboBeanDefinitionParser;
import com.molyfun.core.support.dubbo.spring.AnnotationBean;

/**
 */
public class DubboNamespaceHandler extends com.alibaba.dubbo.config.spring.schema.DubboNamespaceHandler {
	public void init() {
		super.init();
		registerBeanDefinitionParser("annotation", new DubboBeanDefinitionParser(AnnotationBean.class, true));
	}

}
