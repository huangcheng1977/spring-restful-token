package com.molyfun.core.base;

/**

 * 创建日期：2016-7-8下午3:07:23

 * 作者：石冬生

 */

public class TokenThreadLocal {

    private static final ThreadLocal<String> LOCAL = new ThreadLocal<String>();

    public static void set(String token) {
        LOCAL.set(token);
    }

    public static String get() {
        return LOCAL.get();
    }

}