package com.molyfun.core.base;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.molyfun.core.Constants;

import com.molyfun.core.exception.BaseException;
import com.molyfun.core.exception.BusinessException;
import com.molyfun.core.exception.IllegalParameterException;
import com.molyfun.core.support.HttpCode;
import com.molyfun.core.support.MhStudentService;
import com.molyfun.core.util.CommonUtil;
import com.molyfun.core.util.WebUtil;

/**
 * 控制器基类
 */
public abstract class BaseController {
	protected final Logger logger = LogManager.getLogger(this.getClass());


	
	
	/** 获取当前用户Id */
	protected Integer getCurrUser() {
		return WebUtil.getCurrentUser();
	}

	/** 设置成功响应代码 */
	protected ResponseEntity<ModelMap> setSuccessModelMap(ModelMap modelMap) {
		return setSuccessModelMap(modelMap, null);
	}

	/** 设置成功响应代码 */
	protected ResponseEntity<ModelMap> setSuccessModelMap(ModelMap modelMap, Object data) {
		return setModelMap(modelMap, HttpCode.OK, data);
	}
	
	/** 设置成功响应代码 */
	protected ResponseEntity<ModelMap> setSuccessModelMap(Object data) {
		return setModelMap(new ModelMap(), HttpCode.OK, data);
	}

	/** 设置响应代码 */
	protected ResponseEntity<ModelMap> setModelMap(ModelMap modelMap, HttpCode code) {
		return setModelMap(modelMap, code, null);
	}
	
	/** 设置响应代码 */
	protected ResponseEntity<ModelMap> setModelMap(ModelMap modelMap, HttpCode code,String msg) {
		return setModelMap(modelMap, code,msg, null);
	}	
	
	/** 设置响应代码 */
	protected ResponseEntity<ModelMap> setModelMap( HttpCode code,String msg) {
		return setModelMap(new ModelMap(), code,msg, null);
	}	
	
	/** 设置响应代码 */
	protected ResponseEntity<ModelMap> setModelMap(ModelMap modelMap, HttpCode code, Object data) {
		modelMap.remove("void");
		if (data != null) {
			CommonUtil.BeanParamAssembly(data);
			modelMap.put("data", data);
		}
		modelMap.put("code", code.value());
		modelMap.put("msg", code.msg());
		return ResponseEntity.ok(modelMap);
	}

	/** 设置响应代码 */
	protected ResponseEntity<ModelMap> setModelMap(ModelMap modelMap, HttpCode code, String msg,Object data) {
		modelMap.remove("void");
		if (data != null) {
			CommonUtil.BeanParamAssembly(data);
			modelMap.put("data", data);
		}
		modelMap.put("code", code.value());
		modelMap.put("msg", msg);
		return ResponseEntity.ok(modelMap);
	}
	
	

	/** 异常处理 */
	@ExceptionHandler(Exception.class)
	public void exceptionHandler(HttpServletRequest request, HttpServletResponse response, Exception ex)
			throws Exception {
//		logger.error(Constants.Exception_Head, ex);
		ModelMap modelMap = new ModelMap();
		if (ex instanceof BaseException) {
			((BaseException) ex).handler(modelMap);
		} else if (ex instanceof IllegalArgumentException) {
			new IllegalParameterException(ex.getMessage()).handler(modelMap);
		} else if (ex instanceof BusinessException) {
			new BusinessException(ex.getMessage()).handler(modelMap);
		} else if (ex instanceof UnauthorizedException) {
			setModelMap(modelMap, HttpCode.FORBIDDEN);
		} else {
			setModelMap(modelMap, HttpCode.INTERNAL_SERVER_ERROR);
		}
		request.setAttribute("msg", modelMap.get("msg"));
		response.setContentType("application/json;charset=UTF-8");
		byte[] bytes = JSON.toJSONBytes(modelMap, SerializerFeature.DisableCircularReferenceDetect);
		response.getOutputStream().write(bytes);
	}
	

}
